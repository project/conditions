<?php

declare(strict_types=1);

namespace Drupal\conditions_field;

/**
 * The conditions field service interface.
 */
interface ConditionsFieldServiceInterface {

  /**
   * Gets all matching items from database for a given condition configuration.
   *
   * @param string $entity_type_id
   *   The entity_type_id.
   * @param array $condition_configuration
   *   The condition configuration.
   * @param array $contexts
   *   An optional set of contexts to use with the conditions.
   * @param array $additional_wheres
   *   Allows adding more where conditions to the query, like the published
   *   status which will be checked per default to be published.
   *
   * @return array
   *   The matching items by entity_type_id.
   */
  public function getMatchingItems(string $entity_type_id, array $condition_configuration, array $contexts = [], array $additional_wheres = ['status' => 1]): array;

  /**
   * Loads all field storage definitions using the conditions field type.
   *
   * @param string|null $entity_type_id
   *   An optional entity_type_id to check for field conditions.
   *
   * @return array
   *   An array of field definitions. Keyed by entity_type_id and field_name.
   */
  public function getConditionsFieldDefinitions(string $entity_type_id = NULL): array;

  /**
   * Loads the field data for a given condition configuration from database.
   *
   * @param string $entity_type_id
   *   The entity_type_id to load from.
   * @param array $condition_configuration
   *   The condition configuration to check for.
   * @param array $additional_wheres
   *   Allows adding more where conditions to the query, like the published
   *   status which will be checked per default to be published.
   *
   * @return array
   *   The database rows with the condition configuration by entity_type_id.
   */
  public function getItemsDataByConditionConfiguration(string $entity_type_id, array $condition_configuration, array $additional_wheres = ['status' => 1]): array;

}
