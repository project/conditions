<?php

declare(strict_types=1);

namespace Drupal\conditions_field\Plugin\Field\FieldType;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\json_field\Plugin\Field\FieldType\NativeJsonItem;

/**
 * Defines the 'conditions' field type.
 */
#[FieldType(
  id: "conditions",
  label: new TranslatableMarkup("Conditions"),
  description: new TranslatableMarkup("Stores condition configurations as json."),
  category: "data",
  default_widget: "conditions_groups",
  default_formatter: "conditions_empty_formatter",
  constraints: ["valid_json" => []],
)]
class ConditionsItem extends NativeJsonItem implements ConditionsItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['condition_logic'] = [
      'description' => 'The condition logic for the conditions.',
      'type' => 'varchar',
      'length' => 255,
      'binary' => FALSE,
    ];
    $schema['columns']['group_condition_logic'] = [
      'description' => 'The condition logic for the conditions groups.',
      'type' => 'varchar',
      'length' => 255,
      'binary' => FALSE,
    ];
    $schema['columns']['status'] = [
      'description' => 'Whether the condition group is published or not.',
      'type' => 'int',
      'size' => 'tiny',
      'unsigned' => TRUE,
      'default' => 1,
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return empty($value) || empty(JSON::decode($value));
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['condition_logic'] = DataDefinition::create('string')
      ->setLabel(t('The condition logic'));
    $properties['group_condition_logic'] = DataDefinition::create('string')
      ->setLabel(t('The group condition logic'));
    $properties['status'] = DataDefinition::create('integer')
      ->setLabel(t('The status'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    // Since not fields are already used, make sure some default values are set.
    if (!isset($this->group_condition_logic)) {
      $this->group_condition_logic = ConditionsItemInterface::CONDITIONS_GROUP_OPERATOR;
    }
    if (!isset($this->status)) {
      $this->status = ConditionsItemInterface::CONDITIONS_STATUS;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getConditions(): array {
    return JSON::decode($this->value) ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionsOperator(): string {
    return $this->condition_logic ?? 'and';
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionsGroupOperator(): string {
    return $this->group_condition_logic ?? 'or';
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): int {
    return (int) $this->status;
  }

}
