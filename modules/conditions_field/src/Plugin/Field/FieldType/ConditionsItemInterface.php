<?php

declare(strict_types=1);

namespace Drupal\conditions_field\Plugin\Field\FieldType;

/**
 * Defines the conditions item interface.
 */
interface ConditionsItemInterface {

  /**
   * The default status indicating the condition group is published.
   */
  const CONDITIONS_STATUS = 1;

  /**
   * The default operator for conditions groups.
   */
  const CONDITIONS_GROUP_OPERATOR = 'or';

  /**
   * The default operator for conditions.
   */
  const CONDITIONS_OPERATOR = 'and';

  /**
   * Gets and decodes the conditions configuration.
   *
   * @return array
   *   An array of condition configuration values.
   */
  public function getConditions(): array;

  /**
   * Gets the conditions operator from the field item.
   *
   * @return string
   *   The conditions operator. The default value is 'and'.
   */
  public function getConditionsOperator(): string;

  /**
   * Gets the conditions group operator from the field item.
   *
   * @return string
   *   The conditions group operator. The default value is 'or'.
   */
  public function getConditionsGroupOperator(): string;

  /**
   * Checks whether this condition group is published.
   *
   * @return int
   *   Returns 1 if the condition group is published, or 0 otherwise.
   */
  public function getStatus(): int;

}
