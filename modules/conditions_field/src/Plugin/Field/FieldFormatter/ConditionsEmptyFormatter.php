<?php

declare(strict_types=1);

namespace Drupal\conditions_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the 'ConditionsEmptyFormatter' formatter.
 */
#[FieldFormatter(
  id: 'conditions_empty_formatter',
  label: new TranslatableMarkup('Empty formatter'),
  field_types: [
    'conditions',
  ],
)]
class ConditionsEmptyFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Does not actually output anything.
    return [];
  }

}
