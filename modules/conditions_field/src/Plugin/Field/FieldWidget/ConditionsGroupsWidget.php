<?php

declare(strict_types=1);

namespace Drupal\conditions_field\Plugin\Field\FieldWidget;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\conditions_field\Plugin\Field\FieldType\ConditionsItemInterface;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the 'conditions_groups' field widget.
 */
#[FieldWidget(
  id: 'conditions_groups',
  label: new TranslatableMarkup('Conditions groups'),
  description: new TranslatableMarkup('A widget for managing conditions groups.'),
  field_types: ['conditions'],
  multiple_values: TRUE,
)]
class ConditionsGroupsWidget extends WidgetBase {

  /**
   * Constructs a ConditionsGroupsWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Component\Uuid\UuidInterface $uuidService
   *   The UUID service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    protected UuidInterface $uuidService,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['third_party_settings'],
      $container->get('uuid'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $default_value = [];
    foreach ($items as $item) {
      $default_value[$this->uuidService->generate()] = [
        'group_condition_logic' => $item->getConditionsGroupOperator(),
        'condition_logic' => $item->getConditionsOperator(),
        'status' => $item->getStatus(),
        'conditions' => $item->getConditions(),
      ];
    }

    return array_merge([
      '#type' => 'conditions_groups',
      '#default_value' => $default_value,
    ], $element);
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();

    // Extract the values.
    $form_state->cleanValues();
    $conditions_groups = NestedArray::getValue($form_state->getValues(), array_merge($form['#parents'], [$field_name])) ?? [];
    $this->generateItemsByConditionsGroups($items, $conditions_groups, $form, $form_state);
  }

  /**
   * Generates an item for each condition group given.
   *
   * This method helps us to generate field items for each conditions group.
   * This allows us to handle each group individually with its own condition
   * logic and published status without using the multi-value feature of fields
   * directly (for UX reasons).
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field list items.
   * @param array $conditions_groups
   *   The conditions group's to generate items for.
   */
  protected function generateItemsByConditionsGroups(FieldItemListInterface $items, array $conditions_groups, array $form, FormStateInterface $form_state) {
    parent::extractFormValues($items, $form, $form_state);
    $delta = 0;
    foreach ($conditions_groups as $conditions_group) {
      if (isset($conditions_group['conditions'])) {
        // Extract conditions and remove the condition logic.
        $conditions = $conditions_group['conditions'];
        $condition_logic = $conditions['condition_logic'] ?? ConditionsItemInterface::CONDITIONS_OPERATOR;
        unset($conditions['condition_logic']);

        $items->set($delta++, [
          'group_condition_logic' => $conditions_group['group_condition_logic'] ?? ConditionsItemInterface::CONDITIONS_GROUP_OPERATOR,
          'condition_logic' => $condition_logic,
          'status' => $conditions_group['status'] ?? ConditionsItemInterface::CONDITIONS_STATUS,
          'value' => Json::encode($conditions),
        ]);
      }
    }
  }

}
