<?php

declare(strict_types=1);

namespace Drupal\conditions_field\Plugin\Field\FieldWidget;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the 'conditions' field widget.
 */
#[FieldWidget(
  id: 'conditions',
  label: new TranslatableMarkup('Conditions'),
  description: new TranslatableMarkup('A widget for managing conditions.'),
  field_types: ['conditions'],
  multiple_values: TRUE,
)]
class ConditionsWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $default_value = !$items->isEmpty() ? array_merge(
      $items->first()->getConditions(),
      ['condition_logic' => $items->first()->getConditionsOperator()]
    ) : [];

    return array_merge([
      '#type' => 'conditions',
      '#default_value' => $default_value,
    ], $element);
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
    parent::extractFormValues($items, $form, $form_state);
    $field_name = $this->fieldDefinition->getName();

    // Extract the values.
    $form_state->cleanValues();
    $conditions = NestedArray::getValue($form_state->getValues(), array_merge($form['#parents'], [$field_name])) ?? [];
    $condition_logic = $conditions['condition_logic'];
    unset($conditions['condition_logic']);

    $items->setValue([
      'condition_logic' => $condition_logic,
      'value' => JSON::encode($conditions),
    ]);
  }

}
