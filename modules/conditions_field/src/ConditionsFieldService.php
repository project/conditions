<?php

declare(strict_types=1);

namespace Drupal\conditions_field;

use Drupal\Core\Database\Connection;
use Drupal\Component\Serialization\Json;
use Drupal\conditions\ConditionsServiceInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Condition\ConditionAccessResolverTrait;

/**
 * The conditions field service.
 */
class ConditionsFieldService implements ConditionsFieldServiceInterface {

  use ConditionAccessResolverTrait;

  /**
   * Stores the loaded data by entity_type_id and configuration checksum.
   *
   * @var array
   *   The data from database.
   */
  protected array $itemsData = [];

  /**
   * A regex for matching a valid machine name.
   */
  const VALID_ID_REGEX = '/[^a-z0-9_]+/';

  /**
   * Constructs an ConditionsFieldService object.
   *
   * @param string $fieldType
   *   The field type.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   * @param \Drupal\conditions\ConditionsServiceInterface $conditionsService
   *   The conditions service.
   */
  public function __construct(
    protected readonly string $fieldType,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly EntityFieldManagerInterface $entityFieldManager,
    protected readonly Connection $database,
    protected readonly ConditionsServiceInterface $conditionsService,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getMatchingItems(string $entity_type_id, array $condition_configuration, array $contexts = [], array $additional_wheres = ['status' => 1]): array {
    $items_data = $this->getItemsDataByConditionConfiguration($entity_type_id, $condition_configuration, $additional_wheres);

    $matching_items = [];
    foreach ($items_data as $entity_id => $conditions_groups) {
      $this->conditionsService->initializeConditions($conditions_groups, $contexts);
      if ($this->conditionsService->resolveConditionsGroups($conditions_groups)) {
        $matching_items[$entity_id] = [
          'entity_id' => $entity_id,
          'condition_groups' => $conditions_groups,
        ];
      }
    }

    return $matching_items;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionsFieldDefinitions(string $entity_type_id = NULL): array {
    $field_definitions = $this->entityFieldManager->getFieldMapByFieldType($this->fieldType);

    if ($entity_type_id) {
      return $field_definitions[$entity_type_id] ?? [];
    }

    return $field_definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function getItemsDataByConditionConfiguration(string $entity_type_id, array $condition_configuration, array $additional_wheres = ['status' => 1]): array {
    $cache_id = md5(serialize($condition_configuration));

    // Early return if already built.
    if (isset($this->itemsData[$entity_type_id][$cache_id])) {
      return $this->itemsData[$entity_type_id][$cache_id];
    }

    // Check for every possible field.
    foreach ($this->getConditionsFieldDefinitions($entity_type_id) as $field_name => $field_definition) {
      // Better safe than sorry - Make sure the field_name is safe.
      assert(preg_match(self::VALID_ID_REGEX, $field_name) === 0);
      $selects = $wheres = $values = [];
      // Build the query based on the condition configuration.
      foreach ($condition_configuration as $key => $value) {
        // Make sure the key is safe.
        assert(preg_match(self::VALID_ID_REGEX, $key) === 0);
        $selects[] = "JSON_EXTRACT(t.{$field_name}_value, '$**.{$key}')";
        $wheres[] = "JSON_EXTRACT(t.{$field_name}_value, '$**.{$key}') LIKE :$key";
        $values[":$key"] = "%{$value}%";
      }
      foreach ($additional_wheres as $column => $value) {
        assert(preg_match(self::VALID_ID_REGEX, $column) === 0);
        assert(preg_match(self::VALID_ID_REGEX, $value) === 0);
        $wheres[] = "t.{$field_name}_{$column} = {$value}";
      }

      $select = implode(', ', $selects);
      $where = implode(' AND ', $wheres);

      // Get entity_ids where we find a matching condition configuration.
      $table_name = $this->entityTypeManager->getStorage($entity_type_id)->getTableMapping()->getFieldTableName($field_name);
      $result = $this->database->query("
        SELECT entity_id
        FROM {{$table_name}} t
        WHERE {$where}
      ", $values);
      $entity_ids = $result->fetchCol();

      if (empty($entity_ids)) {
        continue;
      }

      // Get all condition groups (rows) for the entity_id that had one matching
      // row.
      $result = $this->database->query("
        SELECT
          entity_id,
          delta,
          {$field_name}_value,
          {$field_name}_condition_logic,
          {$field_name}_group_condition_logic,
          {$field_name}_status,
          $select
        FROM {{$table_name}} t
        WHERE entity_id IN (:entity_ids[])
      ", [':entity_ids[]' => $entity_ids]);

      foreach ($result->fetchAll() as $item) {
        $value = JSON::decode($item->{"{$field_name}_value"});
        $this->itemsData[$entity_type_id][$cache_id][$item->entity_id][$item->delta] = [
          'entity_id' => $item->entity_id,
          'delta' => $item->delta,
          'value' => $value,
          'condition_logic' => $item->{"{$field_name}_condition_logic"},
          'group_condition_logic' => $item->{"{$field_name}_group_condition_logic"},
          'status' => $item->{"{$field_name}_status"},
        ];
      }
    }

    return $this->itemsData[$entity_type_id][$cache_id] ?? [];
  }

}
