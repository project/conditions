<?php

declare(strict_types=1);

namespace Drupal\conditions_test\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a conditions element test form.
 */
final class ConditionsFormTest extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'conditions_form_test';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['conditions'] = [
      '#type' => 'conditions',
      '#title' => $this->t('Conditions'),
      '#default_value' => [],
    ];
    $form['conditions_cardinality'] = [
      '#type' => 'conditions',
      '#title' => $this->t('Conditions with cardinality 2'),
      '#default_value' => [],
      '#cardinality' => 2,
    ];
    $form['conditions_condition_logic_locked'] = [
      '#type' => 'conditions',
      '#title' => $this->t('Conditions with locked condition logic'),
      '#condition_logic' => 'or',
      '#condition_logic_locked' => TRUE,
      '#default_value' => [],
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Send'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $form_state->cleanValues();

    if (\Drupal::moduleHandler()->moduleExists('devel')) {
      \Drupal::service('devel.dumper')->message($form_state->getValue('conditions'));
      \Drupal::service('devel.dumper')->message($form_state->getValue('conditions_cardinality'));
      \Drupal::service('devel.dumper')->message($form_state->getValue('conditions_condition_logic_locked'));
    }
    else {
      \Drupal::messenger()->addStatus(print_r($form_state->getValue('conditions'), TRUE));
      \Drupal::messenger()->addStatus(print_r($form_state->getValue('conditions_cardinality'), TRUE));
      \Drupal::messenger()->addStatus(print_r($form_state->getValue('conditions_condition_logic_locked'), TRUE));
    }
  }

}
