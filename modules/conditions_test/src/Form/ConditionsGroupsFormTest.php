<?php

declare(strict_types=1);

namespace Drupal\conditions_test\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a conditions_groups element test form.
 */
final class ConditionsGroupsFormTest extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'conditions_groups_form_test';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['conditions_groups'] = [
      '#type' => 'conditions_groups',
      '#title' => $this->t('Conditions groups'),
      '#default_value' => [
        'uuid_1' => [
          'status' => 1,
          'conditions' => [
            'uuid_2' => ['id' => 'language', 'configuration' => []],
          ],
        ],
      ],
    ];
    $form['conditions_groups_cardinality'] = [
      '#type' => 'conditions_groups',
      '#title' => $this->t('Conditions groups with cardinality 2'),
      '#default_value' => [],
      '#cardinality' => 2,
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Send'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $form_state->cleanValues();

    if (\Drupal::moduleHandler()->moduleExists('devel')) {
      \Drupal::service('devel.dumper')->message($form_state->getValue('conditions_groups'));
      \Drupal::service('devel.dumper')->message($form_state->getValue('conditions_groups_cardinality'));
    }
    else {
      \Drupal::messenger()->addStatus(print_r($form_state->getValue('conditions_groups'), TRUE));
      \Drupal::messenger()->addStatus(print_r($form_state->getValue('conditions_groups_cardinality'), TRUE));
    }
  }

}
