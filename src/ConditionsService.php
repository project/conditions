<?php

declare(strict_types=1);

namespace Drupal\conditions;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Condition\ConditionAccessResolverTrait;
use Drupal\Core\Condition\ConditionPluginCollection;
use Drupal\Core\Plugin\Context\ContextHandlerInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;

/**
 * The conditions service.
 */
class ConditionsService implements ConditionsServiceInterface {

  use ConditionAccessResolverTrait;

  /**
   * Constructs an ConditionsService object.
   *
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $contextRepository
   *   The lazy context repository service.
   * @param \Drupal\Core\Plugin\Context\ContextHandlerInterface $contextHandler
   *   The ContextHandler for applying contexts to conditions properly.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $conditionManager
   *   The plugin.manager.condition service.
   */
  public function __construct(
    protected readonly ContextRepositoryInterface $contextRepository,
    protected readonly ContextHandlerInterface $contextHandler,
    protected readonly PluginManagerInterface $conditionManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function resolveConditionsGroups(array $conditions_groups): bool {
    // If there are no condition groups return TRUE.
    $default = TRUE;
    foreach ($conditions_groups as $conditions) {
      if ($conditions['group_condition_logic'] === 'or') {
        // If none of the condition groups is fulfilled return FALSE.
        $default = FALSE;
        if ($this->resolveConditions($conditions['conditions'], $conditions['condition_logic'])) {
          // If one condition group is fulfilled return TRUE.
          return TRUE;
        }
      }
      elseif ($conditions['group_condition_logic'] === 'and') {
        // If all the condition groups are fulfilled return TRUE.
        $default = TRUE;
        if (!$this->resolveConditions($conditions['conditions'], $conditions['condition_logic'])) {
          // If one condition group fails return FALSE.
          return FALSE;
        }
      }
    }

    return $default;
  }

  /**
   * {@inheritdoc}
   */
  public function initializeConditions(array &$condition_groups, array $contexts = []): void {
    foreach ($condition_groups as $delta => $condition_group) {
      // Ignore inactive condition groups.
      if (!$condition_group['status']) {
        unset($condition_groups[$delta]);
        continue;
      }
      $conditions_data = $condition_group['value'];
      $conditions = new ConditionPluginCollection($this->conditionManager, $conditions_data);
      foreach ($conditions as $condition) {
        if ($condition instanceof ContextAwarePluginInterface) {
          try {
            $runtime_contexts = $this->contextRepository->getRuntimeContexts(array_values($condition->getContextMapping()));
            $this->contextHandler->applyContextMapping($condition, array_merge($runtime_contexts, $contexts));
          }
          catch (\Exception $e) {
          }
        }
      }

      $condition_groups[$delta]['conditions'] = $conditions;
    }
  }

}
