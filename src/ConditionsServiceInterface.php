<?php

declare(strict_types=1);

namespace Drupal\conditions;

/**
 * The conditions service interface.
 */
interface ConditionsServiceInterface {

  /**
   * Resolves condition groups and their conditions.
   *
   * @param array $conditions_groups
   *   The condition groups.
   *
   * @return bool
   *   TRUE if resolved.
   */
  public function resolveConditionsGroups(array $conditions_groups): bool;

  /**
   * Initializes conditions for a set of condition groups.
   *
   * @param array $condition_groups
   *   The condition groups.
   * @param array $contexts
   *   An optional set of contexts to use with the conditions.
   */
  public function initializeConditions(array &$condition_groups, array $contexts = []): void;

}
