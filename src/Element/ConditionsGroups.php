<?php

declare(strict_types=1);

namespace Drupal\conditions\Element;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Render\Attribute\FormElement;
use Drupal\Core\Render\Element\FormElementBase;

/**
 * Provides a conditions_groups element.
 *
 * Properties:
 * - #cardinality: (Optional) The amount of conditions groups can be added. The
 *   default value is unlimited (-1).
 * - #group_condition_logic: (Optional) Can be either 'and' or 'or' to evaluate
 *   the conditions groups against each other.
 *
 * Usage example:
 * @code
 * $form['my_conditions_groups_element'] = [
 *   '#type' => 'conditions_groups',
 *   '#title' => $this->t('Conditions groups'),
 *   '#conditions_group_title' => $this->t('Conditions group configuration'),
 *   '#cardinality' => -1,
 *   '#group_condition_logic' => 'or',
 *   '#group_condition_logic_locked' => FALSE,
 * ];
 * @endcode
 */
#[FormElement('conditions_groups')]
class ConditionsGroups extends FormElementBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    return [
      '#input' => TRUE,
      '#process' => [
        [$class, 'processConditionsGroupsElement'],
        [$class, 'buildConditionsGroupsForm'],
      ],
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * Processes the conditions group form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processConditionsGroupsElement(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    // Set some default values.
    $element['#cardinality'] = $element['#cardinality'] ?? -1;
    $element['#group_condition_logic'] = $element['#group_condition_logic'] ?? 'or';
    $element['#group_condition_logic_locked'] = $element['#group_condition_logic_locked'] ?? FALSE;
    $element['#conditions_group_title'] = $element['#conditions_group_title'] ?? t('Conditions group configuration');
    $element['#conditions_title'] = $element['#conditions_title'] ?? t('Conditions');

    if ((!isset($form_state->getUserInput()['_drupal_ajax']) && !$form_state->getTriggeringElement())
      || ($form_state->getTriggeringElement() && $form_state->getTriggeringElement()['#ajax']['callback'][0] != static::class)) {
      $conditions_groups = $element['#value'] ?: $element['#default_value'];
    }
    else {
      $form_state->cleanValues();
      $conditions_groups = $form_state->getValue($element['#parents']);
    }

    $element['#conditions_groups'] = [];
    foreach ($conditions_groups as $uuid => $conditions_group) {
      if (isset($conditions_group['conditions'])) {
        $element['#conditions_groups'][$uuid] = $conditions_group;
      }
    }

    // Empty behavior is to show a row of conditions_groups.
    if (empty($element['#conditions_groups']) && (!$form_state->getTriggeringElement() || ($form_state->getTriggeringElement() && !str_starts_with($form_state->getTriggeringElement()['#name'], 'remove-conditions-group')))) {
      // @todo Make this configurable.
      $element['#conditions_groups'][\Drupal::service('uuid')->generate()] = [
        'status' => 1,
        'conditions' => [],
      ];
    }

    $parents = implode('_', $element['#parents']);
    $wrapper_id = Html::getId("conditions-groups-form-element-{$element['#name']}-$parents-wrapper");
    return array_merge($element, [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#wrapper_id' => $wrapper_id,
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
      '#attached' => [
        'library' => [
          'plugin_form_element/drupal.plugins.element',
          'conditions/drupal.conditions-group.element',
        ],
      ],
    ]);
  }

  /**
   * Builds the conditions groups form for the processed element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function buildConditionsGroupsForm(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    foreach ($element['#conditions_groups'] as $uuid => $conditions_group) {
      if (!is_array($conditions_group)) {
        continue;
      }
      // This is a workaround for
      // https://www.drupal.org/project/drupal/issues/2895887
      $logic_options = ['and' => t('And'), 'or' => t('Or')];
      $default_logic_option = $conditions_group['group_condition_logic'] ?? $element['#group_condition_logic'];
      uksort($logic_options, fn($a, $b) => $a === $default_logic_option ? -1 : ($b === $default_logic_option ? 1 : 0));

      $element[$uuid] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['conditions-groups-container']],
      ];

      $element[$uuid]['header'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'conditions-groups-header',
            'container-inline',
          ],
        ],
        '#parents' => array_merge($element['#parents'], [$uuid]),
        '#array_parents' => array_merge($element['#array_parents'], [$uuid]),
      ];
      $element[$uuid]['header']['label'] = [
        '#type' => 'html_tag',
        '#tag' => 'strong',
        '#value' => $element['#conditions_group_title'],
        '#attributes' => ['class' => ['conditions-groups-label']],
      ];
      $element[$uuid]['header']['group_condition_logic'] = [
        '#type' => $element['#group_condition_logic_locked'] ? 'hidden' : 'select',
        '#options' => $logic_options,
        '#default_value' => $conditions_group['group_condition_logic'] ?? $element['#group_condition_logic'],
        '#attributes' => [
          // @todo Add hidden title for a11y.
          'title' => t('Change the conditions group logic'),
        ],
        '#wrapper_attributes' => [
          'class' => ['container-inline'],
        ],
      ];
      $element[$uuid]['header']['status'] = [
        '#type' => 'select',
        '#default_value' => $conditions_group['status'],
        '#options' => [
          1 => t('Published'),
          0 => t('Unpublished'),
        ],
        '#attributes' => [
          // @todo Add hidden title for a11y.
          'title' => t('Change the conditions group status'),
        ],
      ];
      $element[$uuid]['header']['actions'] = [
        '#type' => 'actions',
      ];
      $element[$uuid]['header']['actions']['remove'] = [
        '#type' => 'submit',
        '#value' => t('Remove'),
        '#submit' => [[static::class, 'removeConditionsGroup']],
        '#depth' => -4,
        '#attributes' => [
          'title' => [t('Remove conditions group @i', ['@i' => ((int) $uuid) + 1])],
        ],
        '#ajax' => [
          'callback' => [static::class, 'removeConditionsGroupAjax'],
          'event' => 'click',
          'wrapper' => $element['#wrapper_id'],
          'uuid' => $uuid,
        ],
        '#name' => 'remove-conditions-group-' . $uuid,
        '#button_type' => 'danger',
        '#limit_validation_errors' => [],
        // Only show the remove button if we have more than one item.
        // @todo Make this configurable.
        // '#access' => count($element['#conditions_groups']) > 1,
      ];
      $form_state->addCleanValueKey(array_merge(
        $element['#parents'],
        [$uuid, 'header'])
      );

      $element[$uuid]['conditions'] = [
        '#type' => 'conditions',
        '#title' => $element['#conditions_title'],
        '#default_value' => $conditions_group['conditions'] ?? [],
      ];
      if (isset($conditions_group['condition_logic'])) {
        $element[$uuid]['conditions']['#condition_logic'] = $conditions_group['condition_logic'];
      }
      if (isset($element['#condition_logic_locked'])) {
        $element[$uuid]['conditions']['#condition_logic_locked'] = $element['#condition_logic_locked'];
      }
    }

    if (!static::isDisabled($element)) {
      $element['actions'] = [
        '#type' => 'actions',
        '#weight' => -20,
        '#attributes' => [
          'class' => ['conditions-groups-form-actions'],
        ],
      ];
      $element['actions']['add'] = [
        '#type' => 'submit',
        // @todo Make this configurable.
        '#value' => t('+ Add conditions group'),
        '#submit' => [[static::class, 'addConditionsGroup']],
        '#depth' => -2,
        '#ajax' => [
          'callback' => [static::class, 'addConditionsGroupAjax'],
          'event' => 'click',
          'progress' => ['type' => 'fullscreen'],
          'effect' => 'fade',
          'wrapper' => $element['#wrapper_id'],
        ],
        '#name' => 'add-conditions-group',
        '#limit_validation_errors' => [$element['#array_parents']],
      ];
      $element['actions']['add']['#name'] = Html::getId($element['#wrapper_id'] . '_' . $element['actions']['add']['#name']);
      $element['actions']['add']['#id'] = $element['actions']['add']['#name'];

      $form_state->addCleanValueKey(array_merge($element['#parents'], ['actions']));
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input) {
      foreach ($input as $key => &$value) {
        if (!isset($value['conditions']) && is_array($value)) {
          $value['conditions'] = [];
        }
      }
    }

    return $input ?: [];
  }

  /**
   * Handles adding a new conditions group to the form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public static function addConditionsGroup(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();
    $condition_groups = NestedArray::getValue($form, static::getParents($form_state, TRUE))['#value'];
    $condition_groups[\Drupal::service('uuid')->generate()] = [
      'status' => 1,
      'conditions' => [],
    ];
    $form_state->setValue(static::getParents($form_state), $condition_groups);
    $form_state->setRebuild();
  }

  /**
   * AJAX callback for adding a conditions group.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The updated form fragment.
   */
  public static function addConditionsGroupAjax(array &$form, FormStateInterface $form_state): array {
    return NestedArray::getValue($form, static::getParents($form_state, TRUE));
  }

  /**
   * Handles removing a conditions group from the form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public static function removeConditionsGroup(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();
    $conditions_groups = NestedArray::getValue($form, static::getParents($form_state, TRUE))['#value'];
    unset($conditions_groups[$form_state->getTriggeringElement()['#ajax']['uuid']]);
    $form_state->setValue(array_slice($form_state->getTriggeringElement()['#parents'], 0, -3), $conditions_groups);
    $form_state->setRebuild();
  }

  /**
   * AJAX callback for removing a conditions group.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The updated form fragment.
   */
  public static function removeConditionsGroupAjax(array &$form, FormStateInterface $form_state): array {
    return NestedArray::getValue($form, static::getParents($form_state, TRUE));
  }

  /**
   * Determines if adding more conditions groups is disabled.
   *
   * @param array $element
   *   The form element.
   *
   * @return bool
   *   TRUE if adding plugins is disabled, FALSE otherwise.
   */
  protected static function isDisabled(array $element): bool {
    return $element['#cardinality'] !== -1 && count($element['#conditions_groups']) >= $element['#cardinality'];
  }

  /**
   * Gets the parent elements for form state operations.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param bool $array_parents
   *   Whether to retrieve array parents.
   *
   * @return array|null
   *   The parent elements or NULL.
   */
  protected static function getParents(FormStateInterface $form_state, bool $array_parents = FALSE): ?array {
    if ($triggering_element = $form_state->getTriggeringElement()) {
      return array_slice(
        $array_parents ? $triggering_element['#array_parents'] : $triggering_element['#parents'],
        0,
        $triggering_element['#depth']
      );
    }

    return NULL;
  }

}
