<?php

declare(strict_types=1);

namespace Drupal\conditions\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Attribute\FormElement;
use Drupal\plugin_form_element\Element\Plugins;

/**
 * Provides a conditions form element.
 *
 * This builds a plugin form for condition plugins. It does so by extending the
 * plugin form element with an additional condition logic field which can be
 * used to evaluate conditions in an 'and' or 'or' relation.
 *
 * Properties:
 * - #default_value: Must not be NULL. Plugin configuration stores the plugin's
 *   id inside its configuration, and we need this to initialize the plugin
 *   itself. Therefore, it must at least contain the id of the plugin.
 *
 * Usage example:
 * @code
 * $form['my_conditions_element'] = [
 *   '#type' => 'plugin',
 *   '#title' => $this->t('Configure conditions'),
 *   '#condition_logic' => 'and',
 *   '#condition_logic_locked' => TRUE,
 *   '#default_value' => [[
 *     'id' => 'language',
 *     'langcodes' => ['en' => 'en'],
 *   ]],
 * ];
 * @endcode
 */
#[FormElement('conditions')]
class Conditions extends Plugins {

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    $class = static::class;
    return [
      '#input' => TRUE,
      '#process' => [
        [$class, 'processConditionsElement'],
        [$class, 'processPluginsElement'],
        [$class, 'buildPluginsForm'],
        [$class, 'buildConditionsElement'],
      ],
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * Processes the conditions form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processConditionsElement(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    $element['#plugin_manager'] = 'plugin.manager.condition';
    $element['#attributes']['class'][] = 'conditions-form-element';
    $element['#condition_logic'] = $element['#condition_logic'] ?? 'and';
    $element['#condition_logic_locked'] = $element['#condition_logic_locked'] ?? FALSE;

    return $element;
  }

  /**
   * Builds the conditions form for the processed element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function buildConditionsElement(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    $element['plugin_group']['condition_logic'] = [
      '#title' => t('Condition logic'),
      '#type' => $element['#condition_logic_locked'] ? 'hidden' : 'select',
      '#options' => ['and' => t('And'), 'or' => t('Or')],
      '#default_value' => $element['#condition_logic'],
      '#wrapper_attributes' => [
        'class' => ['container-inline', 'condition-logic-wrapper'],
      ],
      '#parents' => array_merge($element['#parents'], ['condition_logic']),
      '#attached' => [
        'library' => ['conditions/drupal.conditions.element'],
      ],
    ];

    return $element;
  }

}
